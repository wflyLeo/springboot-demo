# 设置项

#### 简介
网上的教程很多是通篇废话，互相抄袭，在此将其总结出最简单最实用的方法，服务器版本CentOS 7.4

#### 服务器命令
1. systemctl status firewalld 查看firewalld状态
2. systemctl start firewalld 开启防火墙
3. systemctl stop firewalld 关闭防火墙
4. systemctl enable firewalld 开机启用防火墙
5. firewall-cmd --reload 重新载入
6. firewall-cmd --zone= public --query-port=6379/tcp 查看6379端口
7. firewall-cmd --permanent --zone=public --add-port=6379/tcp 打开6379端口
8. firewall-cmd --permanent --zone=public --remove-port=6379/tcp 关闭6379端口

#### Redis
1. 安装 yum install redis，安装后配置文件的地址在/etc/redis.conf
2. 设置密码：redis.conf中，修改取消 #requirepass password 注释，修改密码例 requirepass 123456
3. 把 protected-mode yes 改为 protected-mode no
4. 注释掉 bind 127.0.0.1 可以使所有的ip访问redis
5. 记得打开6379端口
6. redis-server /etc/redis.conf 通过配置文件启动redis
7. redis-cli -a password 带密码进入redis-cli
8. shutdown 在cli中关闭redis

#### Nginx
1. 安装 yum install nginx，安装后配置文件的地址在/etc/nginx/nginx.conf
2. nginx 启动
3. nginx -s stop 快速关闭； nginx -s quit 正常关闭
4. nginx -s reload 重启
5. nginx -t -c /etc/nginx/nginx.conf 判断配置文件是否正确