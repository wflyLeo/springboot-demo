import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';

import {BaiduMapRoutingModule} from './baidu-map-routing.module';

import {BaiduMapComponent} from './baidu-map.component';
import {MapPcComponent} from './pc/map-pc.component';
import {MapMobileComponent} from './mobile/map-mobile.component';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    BaiduMapRoutingModule
  ],
  declarations: [
    BaiduMapComponent,
    MapPcComponent,
    MapMobileComponent
  ],
})

export class BaiduMapModule {
}
