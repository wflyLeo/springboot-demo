import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

declare var BMap;
declare var BMAP_ANCHOR_BOTTOM_RIGHT;
declare var BMAP_ANCHOR_BOTTOM_LEFT;

@Component({
  selector: 'app-baidu-map-mobile',
  templateUrl: './map-mobile.component.html',
  styleUrls: ['./map-mobile.component.css']
})
export class MapMobileComponent implements OnInit {

  BMapContainerHeight = (window.innerHeight - 64) + 'px';
  bmap: any;

  constructor() {
  }

  ngOnInit() {
    this.windowResize();
    window.addEventListener('resize', () => {
      this.windowResize();
    });
    $.getScript('http://api.map.baidu.com/api?ak=PgNHPqzIRsGR1yhs8nkoCtXOZLbwmawp&type=lite&v=1.0').done(() => {
      this.bmap = this.bmapInit();
    });

  }

  bmapInit(): any {
    const map = new BMap.Map('BMap');
    const point = new BMap.Point(114.306412, 30.597472);
    map.centerAndZoom(point, 12);
    const zoomCtrl = new BMap.ZoomControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT, offset: new BMap.Size(20, 20)});
    map.addControl(zoomCtrl);
    const scaleCtrl = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT, offset: new BMap.Size(10, 40)});
    map.addControl(scaleCtrl);
    return map;
  }

  windowResize(): void {
    this.BMapContainerHeight = (window.innerHeight - 64) + 'px';
  }

}
