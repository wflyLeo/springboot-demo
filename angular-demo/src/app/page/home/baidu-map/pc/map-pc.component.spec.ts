import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapPcComponent } from './map-pc.component';

describe('ToolComponent', () => {
  let component: MapPcComponent;
  let fixture: ComponentFixture<MapPcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapPcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapPcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
