import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

declare var BMap;
declare var BMAP_NORMAL_MAP;
declare var BMAP_HYBRID_MAP;
declare var BMAP_ANCHOR_BOTTOM_RIGHT;
declare var BMAP_ANCHOR_BOTTOM_LEFT;

@Component({
  selector: 'app-baidu-map-pc',
  templateUrl: './map-pc.component.html',
  styleUrls: ['./map-pc.component.css']
})
export class MapPcComponent implements OnInit {

  BMapContainerHeight = (window.innerHeight - 64) + 'px';
  bmap: any;

  constructor() {
  }

  ngOnInit() {
    this.windowResize();
    window.addEventListener('resize', () => {
      this.windowResize();
    });
    $.getScript('http://api.map.baidu.com/getscript?v=3.0&ak=PgNHPqzIRsGR1yhs8nkoCtXOZLbwmawp&services=&t=20181029164750').done(() => {
      this.bmap = this.bmapInit();
    });

  }

  bmapInit(): any {
    const map = new BMap.Map('BMap', {enableMapClick: false}); // 禁用点击显示标注详细信息
    map.centerAndZoom(new BMap.Point(114.294338, 30.548967), 12); // 初始化地图,设置中心点坐标和地图级别
    // 添加地图类型控件
    map.addControl(new BMap.MapTypeControl({
      mapTypes: [
        BMAP_NORMAL_MAP,
        BMAP_HYBRID_MAP,
      ]
    }));
    // 左下角，添加比例尺
    map.addControl(new BMap.ScaleControl({
      anchor: BMAP_ANCHOR_BOTTOM_LEFT,
    }));
    map.addControl(new BMap.NavigationControl()); // 左上角，添加默认缩放平移控件
    map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
    map.disableDoubleClickZoom(); // 禁止双击放大
    map.enableKeyboard(); // 开启键盘控制功能
    // 右下角添加小地图
    map.addControl(new BMap.OverviewMapControl({
      isOpen: true,
      anchor: BMAP_ANCHOR_BOTTOM_RIGHT,
    }));
    return map;
  }

  windowResize(): void {
    this.BMapContainerHeight = (window.innerHeight - 64) + 'px';
  }

}
