import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ElementRef} from '@angular/core';
import {AuthService} from '../../auth/auth.service';

import {UserBean} from '../../entity/bean/user.bean';
import {UserFactory} from '../../factory/user.factory';
import {SharedService} from '../../shared/shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AuthService]
})
export class HomeComponent implements OnInit {

  userBean: UserBean = UserFactory.getUser();
  isCollapsed = false;
  defaultSiderWidth = 200;
  sliderTriggerStatus = true;
  sliderStyleGroup: any = {
    flex: `0 0 ${this.defaultSiderWidth}px`,
    maxWidth: `${this.defaultSiderWidth}px`,
    minWidth: `${this.defaultSiderWidth}px`,
    width: `${this.defaultSiderWidth}px`,
  };
  triggerBtnDisplay: any = {
    'fold': '',
    'unfold': 'none',
  };
  contentContainerStyleGroup: any = {
    height: '100px',
    marginLeft: `${this.defaultSiderWidth}px`,
  };

  contentMinHeight = '360px';

  constructor(private ef: ElementRef,
              private router: Router,
              private sharedService: SharedService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.windowResize();
    window.addEventListener('resize', () => {
      this.windowResize();
    });
    this.router.events.subscribe(event => {
      this.sharedService.pub('PageContentMinHeight', parseFloat(this.contentMinHeight));
    });
  }

  windowResize(): void {
    this.contentContainerStyleGroup.height = window.innerHeight + 'px';
    this.contentMinHeight = (window.innerHeight - 64) + 'px';
    if (window.innerWidth <= 1024) {
      this.isCollapsed = true;
    } else {
      this.isCollapsed = false;
    }
    const siderWidth = this.isCollapsed ? 80 : this.defaultSiderWidth;
    this.contentContainerStyleGroup.marginLeft = `${siderWidth}px`;
    // 发布容器高度
    this.sharedService.pub('PageContentMinHeight', parseFloat(this.contentMinHeight));
  }

  sliderTrigger(): void {
    this.sliderTriggerStatus = !this.sliderTriggerStatus;
    const btn = this.ef.nativeElement.querySelector('.ant-layout-sider-trigger');
    if (this.sliderTriggerStatus) {
      // 显示
      const siderWidth = this.isCollapsed ? 80 : this.defaultSiderWidth;
      btn.style.width = `${siderWidth}px`;
      this.contentContainerStyleGroup.marginLeft = `${siderWidth}px`;
      this.sliderStyleGroup.flex = `0 0 ${siderWidth}px`;
      this.sliderStyleGroup.maxWidth = `${siderWidth}px`;
      this.sliderStyleGroup.minWidth = `${siderWidth}px`;
      this.sliderStyleGroup.width = `${siderWidth}px`;
      this.triggerBtnDisplay['fold'] = '';
      this.triggerBtnDisplay['unfold'] = 'none';
    } else {
      // 隐藏
      btn.style.width = '0';
      this.contentContainerStyleGroup.marginLeft = '0';
      this.sliderStyleGroup.flex = '0 0 0px';
      this.sliderStyleGroup.maxWidth = '0';
      this.sliderStyleGroup.minWidth = '0';
      this.sliderStyleGroup.width = '0';
      this.triggerBtnDisplay['fold'] = 'none';
      this.triggerBtnDisplay['unfold'] = '';
    }
  }

  collapsedChange(): void {
    const siderWidth = this.isCollapsed ? 80 : this.defaultSiderWidth;
    this.contentContainerStyleGroup.marginLeft = `${siderWidth}px`;
  }

  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('login');
  }

}
