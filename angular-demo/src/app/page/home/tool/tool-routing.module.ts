import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../../auth/auth.guard';

import {ToolComponent} from './tool.component';
import {IcoGeneratorComponent} from './ico-generator/ico-generator.component';

const routes: Routes = [
  {path: '', redirectTo: 'ico-generator', pathMatch: 'full', canActivate: [AuthGuard]},
  {
    path: '', component: ToolComponent, children: [
      {path: 'ico-generator', component: IcoGeneratorComponent, canActivate: [AuthGuard]},
    ], canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ToolRoutingModule {
}
