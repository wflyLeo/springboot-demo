import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';
import {ToolRoutingModule} from './tool-routing.module';

import {ToolComponent} from './tool.component';
import {IcoGeneratorComponent} from './ico-generator/ico-generator.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    ToolRoutingModule
  ],
  declarations: [
    ToolComponent,
    IcoGeneratorComponent,
  ],
  providers: [
    {provide: NZ_I18N, useValue: zh_CN}
  ]
})

export class ToolModule {
}
