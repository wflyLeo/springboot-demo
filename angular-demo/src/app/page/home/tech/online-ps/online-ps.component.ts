import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../../../shared/shared.service';

@Component({
  selector: 'app-tech-online-ps',
  templateUrl: './online-ps.component.html',
  styleUrls: ['./online-ps.component.css']
})
export class OnlinePsComponent implements OnInit {

  frameStatus = false; // false普通大小；true最大化
  frameStatusValue = '最大化';

  frameStyleGroup: any = {
    position: 'relative',
    left: '0',
    top: '0',
    width: '100%',
    height: '100px',
    zIndex: '0',
  };

  heightCache = '100px';

  constructor(private sharedService: SharedService) {
    sharedService.sub('ContentDivSize').subscribe(res => {
      if (!this.frameStatus) {
        this.frameStyleGroup.height = (parseFloat(res.msg.height) - 5) + 'px';
        this.heightCache = this.frameStyleGroup.height;
      }
    });
  }

  ngOnInit() {
    this.windowResize();
    window.addEventListener('resize', () => {
      this.windowResize();
    });
    document.getElementById('page-item-name').innerText = '在线PS';
  }

  windowResize(): void {
    if (this.frameStatus) {
      this.frameStyleGroup.width = window.innerWidth + 'px';
      this.frameStyleGroup.height = window.innerHeight + 'px';
    }
  }

  changeFrameSize(): void {
    this.frameStatus = !this.frameStatus;
    if (this.frameStatus) {
      // 最大化
      this.frameStyleGroup.position = 'fixed';
      this.frameStyleGroup.width = window.innerWidth + 'px';
      this.frameStyleGroup.height = window.innerHeight + 'px';
      this.frameStyleGroup.zIndex = '5';
      this.frameStatusValue = '普通';
    } else {
      // 普通大小
      this.frameStyleGroup.position = 'relative';
      this.frameStyleGroup.width = '100%';
      this.frameStyleGroup.height = this.heightCache;
      this.frameStyleGroup.zIndex = '0';
      this.frameStatusValue = '最大化';
    }
  }

}
