import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OnlinePsComponent} from './online-ps.component';

describe('ToolComponent', () => {
  let component: OnlinePsComponent;
  let fixture: ComponentFixture<OnlinePsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OnlinePsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlinePsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
