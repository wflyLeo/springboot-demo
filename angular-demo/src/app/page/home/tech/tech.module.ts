import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';
import {TechRoutingModule} from './tech-routing.module';

import {TechComponent} from './tech.component';
import {OnlinePsComponent} from './online-ps/online-ps.component';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    TechRoutingModule
  ],
  declarations: [
    TechComponent,
    OnlinePsComponent,
  ],
  providers: [
    {provide: NZ_I18N, useValue: zh_CN}
  ]
})

export class TechModule {
}
