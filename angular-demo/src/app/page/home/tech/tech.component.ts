import {Component, OnInit} from '@angular/core';
import {ElementRef} from '@angular/core';
import {SharedService} from '../../../shared/shared.service';

@Component({
  selector: 'app-tool',
  templateUrl: './tech.component.html',
  styleUrls: ['./tech.component.css']
})
export class TechComponent implements OnInit {

  contentMinHeight;

  constructor(private ef: ElementRef,
              private sharedService: SharedService) {
    sharedService.sub('PageContentMinHeight').subscribe(res => {
      this.contentMinHeight = (res.msg - 41 - 69) + 'px';
    });
  }

  ngOnInit() {
    this.windowResize();
    setTimeout(() => {
      this.windowResize();
    });
    window.addEventListener('resize', () => {
      this.windowResize();
    });
  }

  windowResize(): void {
    const contentDivHeight = this.ef.nativeElement.querySelector('#content-div').style.minHeight;
    this.sharedService.pub('ContentDivSize', {height: contentDivHeight});
  }

}
