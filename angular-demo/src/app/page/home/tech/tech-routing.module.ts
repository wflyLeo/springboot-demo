import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../../auth/auth.guard';

import {TechComponent} from './tech.component';
import {OnlinePsComponent} from './online-ps/online-ps.component';

const routes: Routes = [
  {path: '', redirectTo: 'online-ps', pathMatch: 'full', canActivate: [AuthGuard]},
  {
    path: '', component: TechComponent, children: [
      {path: 'online-ps', component: OnlinePsComponent, canActivate: [AuthGuard]},
    ], canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TechRoutingModule {
}
