import {Injectable} from '@angular/core';
import {HttpClientService} from '../../http/http-client.service';

import {UserVo} from '../../entity/vo/user.vo';

@Injectable()
export class LoginRegistryService {

  constructor(private http: HttpClientService) {
  }

  registry(user: UserVo) {
    return this.http.request({
      method: 'POST',
      url: '/auth/register',
      data: user
    });
  }

}
