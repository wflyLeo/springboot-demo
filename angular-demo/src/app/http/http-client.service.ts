import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {HttpParamsEntity} from '../entity/entity/http-params.entity';

@Injectable()
export class HttpClientService {

  constructor(private http: HttpClient) {
  }

  public request(params: HttpParamsEntity): any {
    switch (params.method.toUpperCase()) {
      case 'GET':
        return this.get(params.url, params.data === null ? {} : params.data);
        break;
      case 'POST':
        return this.post(params.url, params.data === null ? {} : params.data);
        break;
      default:
        break;
    }
  }

  private get(url: string, params: any): any {
    return this.http.get(url, params)
      .toPromise()
      .then(result => result);
  }

  private post(url: string, params: any) {
    return this.http.post(url, params)
      .toPromise()
      .then(result => result);
  }

}
