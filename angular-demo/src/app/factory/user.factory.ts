import {UserBean} from '../entity/bean/user.bean';
import {UserEntity} from '../entity/entity/user.entity';

export class UserFactory {

  static getUser(): UserBean {
    const storage = localStorage.getItem('login_info');
    if (storage !== null && storage !== '') {
      const loginInfo = JSON.parse(storage);
      const userEntity: UserEntity = loginInfo['userInfo'];
      return new UserBean(userEntity);
    } else {
      return null;
    }
  }

}
