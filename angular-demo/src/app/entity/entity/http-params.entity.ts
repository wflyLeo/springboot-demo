export class HttpParamsEntity {

  method: string;
  url: string;
  data: object;

  constructor(method, url, data) {
    this.method = method;
    this.url = url;
    this.data = data;
  }

}
