import {UserEntity} from '../entity/user.entity';

export class UserBean {

  username: string;
  password: string;
  roles: Array<string>;
  nickname: string;
  tel: string;

  constructor(userEntity: UserEntity) {
    this.username = userEntity.username;
    this.password = userEntity.password;
    if (userEntity.roles !== null && userEntity.roles !== '') {
      this.roles = userEntity.roles.split(',');
    }
    this.nickname = userEntity.nickname;
    this.tel = userEntity.tel;
  }

}
