package com.example.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.common.ResponseData;
import com.example.controller.base.BaseController;

@RestController
@RequestMapping("/test")
public class TestController extends BaseController {

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "hello", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData test() {
		ResponseData responseData = new ResponseData();
		responseData.setMsg("hello,this is a springboot demo");
		responseData.setRetcode(SUCCESS);
		return responseData;
	}

}
