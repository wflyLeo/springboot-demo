package com.example.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.common.ResponseData;
import com.example.common.StatusText;
import com.example.controller.base.BaseController;
import com.example.entity.bean.UserBean;
import com.example.entity.entity.UserEntity;
import com.example.redis.service.RedisLoginService;
import com.example.service.UserService;

@CrossOrigin
@RestController
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private RedisLoginService redisLoginService;

	@RequestMapping(value = "${jwt.route.authentication.login}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData login(HttpServletRequest request, @RequestBody UserBean user) throws AuthenticationException {
		ResponseData responseData = new ResponseData();
		HashMap<String, Object> map = userService.login(user.getUsername(), user.getPassword());
		if (map == null) {
			responseData.setMsg("登录失败，用户名或密码错误");
			responseData.setRetcode(FAIL);
		} else {
			redisLoginService.login(((UserEntity) map.get("user")).getUsername(), (String) map.get("token"));
			responseData.setMsg("登录成功");
			responseData.setRetcode(SUCCESS);
			responseData.setData(map);
		}
		return responseData;
	}

	@RequestMapping(value = "${jwt.route.authentication.register}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData register(@RequestBody UserBean user) throws AuthenticationException {
		ResponseData responseData = new ResponseData();
		switch (userService.register(user)) {
		case StatusText.USER_REGISTER_SUCCESS:
			responseData.setMsg("注册成功");
			responseData.setRetcode(SUCCESS);
			break;
		case StatusText.USER_REGISTER_EXIST:
			responseData.setMsg("该用户已存在");
			responseData.setRetcode(FAIL);
			break;
		case StatusText.USER_REGISTER_FAIL:
			responseData.setMsg("注册失败");
			responseData.setRetcode(FAIL);
			break;
		default:
			break;
		}
		return responseData;
	}

	@GetMapping(value = "${jwt.route.authentication.refresh}")
	public String refreshToken(@RequestHeader String authorization) throws AuthenticationException {
		return userService.refreshToken(authorization);
	}

}
