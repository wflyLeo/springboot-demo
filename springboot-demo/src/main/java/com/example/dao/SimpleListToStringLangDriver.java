package com.example.dao;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;
import org.apache.ibatis.session.Configuration;

public class SimpleListToStringLangDriver extends XMLLanguageDriver implements LanguageDriver {

	private final Pattern inPattern = Pattern.compile("list:#\\{.*\\}");

	@Override
	public SqlSource createSqlSource(Configuration configuration, String script, Class<?> parameterType) {
		Matcher matcher = inPattern.matcher(script);
		if (matcher.find()) {
			
			script = matcher.replaceAll(
					"" + "<foreach item=\"_item\" collection=\"user.roles\" open=\"'\" separator=\",\" close=\"'\">"
							+ "#{_item}" + "</foreach>");
		}
		script = "<script>" + script + "</script>";
		SqlSource sql = super.createSqlSource(configuration, script, parameterType);
		return sql;
	}

}
