package com.example.common;

public class ResponseData {

	private int retcode;
	private Object data;
	private String msg;

	public int getRetcode() {
		return retcode;
	}

	public void setRetcode(int retcode) {
		this.retcode = retcode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "ResponseData [retcode=" + retcode + ", data=" + data + ", msg=" + msg + "]";
	}

}
